# Web-based Environmental Telemetry for M.O.I.S.T.
*W.E.T.* is the RESTful web service component of **M.O.I.S.T.**.
It is written in [Go](https://golang.org/) and provides access to both live
measurements and relevant long-term statistics.
