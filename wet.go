package main

import (
	"log"
	"os"
)

func main() {
	logger := log.New(os.Stdout, "[main] ", 0)
	logger.Println("Web-based Environmental Telemetry for M.O.I.S.T.")
}
